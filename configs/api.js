var express = require('express');
var app = express();
exports.app = app;
var bodyParser = require('body-parser');
var authRouter = require('../lib/auth');
var rightsRouter = require('../lib/rights');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/users', authRouter);
app.use('/rights', rightsRouter);
