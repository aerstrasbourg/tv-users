var should = require('should');
var request = require('supertest');
var api = require('../configs/api').app;
var db = require('../configs/database');

var User = require('../lib/models/user');

describe('/users', function() {
  before(function(done) {
    db.connect(done);
  });

  after(function(done) {
    db.disconnect(done);
  });

  describe('/auth', function() {
    before(function() {
      var thomas = new User({
        login: 'thomas.cruzol@epitech.eu',
        password: 'harrypotter',
        token: 'jesuisuntoken'
      });
      thomas.save();
    });

    after(function() {
      User.find({}).remove();
    });

    describe('POST', function() {
      it('should correctly authenticate user', function(done) {
        request(api)
          .post('/users/auth')
          .send({
            login: 'thomas.cruzol@epitech.eu',
            password: 'harrypotter'
          })
          .expect(200)
          .end(function(err, res) {
            if (err)
              return done(err);
            should(res.body.token).be.a.String;
            done();
          });
      });

      it('should fails to authenticate with bad password', function(done) {
        request(api)
          .post('/users/auth')
          .send({
            login: 'thomas.cruzol@epitech.eu',
            password: 'badpassword'
          })
          .expect(401)
          .end(function(err, res) {
            if (err)
              return done(err);
            done();
          });
      });

      it('should fails to authenticate with bad params', function(done) {
        request(api)
          .post('/users/auth')
          .send({
            password: 'badpassword'
          })
          .expect(400)
          .end(function(err, res) {
            if (err)
              return done(err);
            done();
          });
      });

      it('should fails to authenticate with bad login', function(done) {
        request(api)
          .post('/users/auth')
          .send({
            login: 'badlogin',
            password: 'harrypotter'
          })
          .expect(401)
          .end(function(err, res) {
            if (err)
              return done(err);
            done();
          });
      });
    });
  });
});
