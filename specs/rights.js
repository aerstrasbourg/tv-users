var should = require('should');
var request = require('supertest');
var nock = require('nock');
var api = require('../configs/api').app;
var db = require('../configs/database');

var User = require('../lib/models/user');

describe('/rights', function() {
  before(function(done) {
    db.connect(done);
  });

  after(function(done) {
    db.disconnect(done);
  });

  describe('/check', function() {
    before(function() {
      var thomas = new User({
        token: 'thomastoken',
        roles: ['adm']
      });
      thomas.save();
      var melanie = new User({
        token: 'melanietoken',
        roles: ['adm']
      });
      melanie.save();
      var nau = new User({
        token: 'token',
        roles: ['aer']
      });
      nau.save();
      nock('http://localhost')
        .get('/roles/check', {
          method: 'post',
          url: '/test/ok',
          roles: ['aer']
        })
        .reply(204);
      nock('http://localhost')
        .get('/roles/check', {
          method: 'get',
          url: '/test/ok',
          roles: ['aer']
        })
        .reply(403);
    });

    it('should successfully grant rights', function(done) {
      request(api)
        .get('/rights/check')
        .send({
          url: '/test/ok',
          method: 'post',
          token: "token"
        })
        .expect(204)
        .end(function(err, res) {
          if (err)
            return done(err);
          done();
        });
    });

    it('should successfully deny rights', function(done) {
      request(api)
        .get('/rights/check')
        .send({
          url: '/test/ok',
          method: 'get',
          token: "token"
        })
        .expect(403)
        .end(function(err, res) {
          if (err)
            return done(err);
          done();
        });
    });
  });
});
