# TV-Users
## Description
This is the microservice used to authenticate and manage users for the TV Project.
## API
### Authentification
#### POST on /users/auth
POST on /users/auth with a json like this:
```json
{
  "login": "thomas.cruzol@epitech.eu",
  "password": "harrypotter"
}
```
You will receive a response like this:
```json
{
  "token": "jesuisuntoken"
}
```
### Rights
#### GET /rights/check
GET on /rights/check with a json like this:
```json
{
  "token": "tokenstring",
  "url": "regex",
  "method": "regex"
}
```
You will receive a 403 response if you don't have rights, else 204.
