var express = require('express');
var request = require('superagent');
var app = require('../configs/api').app;
var db = require('../configs/database');
var externals = require('../configs/externals');

var User = require('./models/user');

var rightsUrl = 'http://' + externals.rightsIp;

var router = express.Router();

router.get('/check', function(req, res) {
  if (!req.body.method || !req.body.url || !req.body.token) {
    res.status(400);
    res.end();
  } else {
    User.findOne({token: req.body.token}, function(error, user) {
      if (error) {
        res.status(500);
        res.end();
      } else if (!user) {
        res.status(404);
        res.end();
      } else {
        request
          .get(rightsUrl + '/roles/check')
          .send({
            roles: user.roles,
            url: req.body.url,
            method: req.body.method
          })
          .end(function(err, r) {
            if (err && r.status != 403)
              res.status(500);
            else {
              if (r.status == 204)
                res.status(204);
              else
                res.status(403);
            }
            res.end();
          });
      }
    });
  }
});

module.exports = router;
