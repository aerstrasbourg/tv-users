var express = require('express');
var app = require('../configs/api').app;
var db = require('../configs/database');

var User = require('./models/user');

var router = express.Router();

router.post('/auth', function(req, res) {
  if (!req.body.login || !req.body.password) {
    res.status(400);
    res.end();
  } else {
    User.findOne({login: req.body.login}, function(err, user) {
      if (err) {
        res.status(500);
        res.end();
      } else {
        if (!user) {
          res.status(401);
          res.end();
        }
        else if (req.body.password != user.password) {
          res.status(401);
          res.end();
        }
        else {
          res.status(200);
          res.send({
            token: user.token
          });
        }
      }
    });
  }
});

module.exports = router;
