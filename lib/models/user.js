var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
  firstname: String,
  lastname: String,
  username: String,
  login: String,
  password: String,
  token: String,
  roles: [String]
});

var User = mongoose.model('User', UserSchema);

module.exports = User;
